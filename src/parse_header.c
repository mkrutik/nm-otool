/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_header.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:27:28 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:28:28 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

BOOL	need_cng_end(uint32_t magic)
{
	if (magic == MH_CIGAM || magic == MH_CIGAM_64
		|| magic == FAT_CIGAM || magic == FAT_CIGAM_64)
	{
		return (TRUE);
	}
	return (FALSE);
}

int		processing(t_all_info *inf, const void *ptr)
{
	uint32_t	magic;

	if ((ptr + SARMAG) > inf->f.end_data)
		return (error(ERROR_FSIZE, inf->f.file_name));
	magic = *((uint32_t *)ptr);
	inf->swap = need_cng_end(magic);
	if ((magic == MH_MAGIC || magic == MH_CIGAM) && (inf->arc = X86))
		mach86(inf, ptr);
	else if ((magic == MH_MAGIC_64 || magic == MH_CIGAM_64) && (inf->arc = X64))
		mach64(inf, ptr);
	else if ((magic == FAT_MAGIC || magic == FAT_CIGAM) && (inf->arc = X86))
		fat86(inf);
	else if ((magic == FAT_MAGIC_64 || magic == FAT_CIGAM_64) &&
			(inf->arc = X64))
		fat64(inf);
	else if (!ft_strncmp((char*)(inf->f.d), (char*)ARMAG, SARMAG))
		archive(inf);
	else
		return (error(ERROR_UN_MAG, inf->f.file_name));
	return (EXIT_SUCCESS);
}
