/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprint_nm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:24:24 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:25:25 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		print_hex(uint64_t addr, size_t align)
{
	const char *hex;

	hex = "0123456789abcdef";
	if (align == 0)
		return ;
	print_hex(addr / 16, align - 1);
	write(STDOUT_FILENO, &hex[addr % 16], 1);
}

static void	print_sect_symbol(t_sym_row *symb, t_sect *sec, uint8_t arch)
{
	print_hex(symb->addr, arch == X64 ? 16 : 8);
	if (symb->sym_type == N_ABS)
		ft_putstr((symb->ext ? " A " : " a "));
	else if (symb->sym_type == N_INDR)
		ft_putstr((symb->ext ? " I " : " i "));
	else if (symb->sym_type == N_PBUD)
		ft_putstr("u ");
	else if (symb->sym_type == N_SECT)
	{
		if (symb->sec_num == sec->bss)
			ft_putstr((symb->ext ? " B " : " b "));
		else if (symb->sec_num == sec->text)
			ft_putstr((symb->ext ? " T " : " t "));
		else if (symb->sec_num == sec->data)
			ft_putstr((symb->ext ? " D " : " d "));
		else
			ft_putstr((symb->ext ? " S " : " s "));
	}
}

static void	print_nm(t_all_info *inf)
{
	size_t	i;

	i = 0;
	while (i < inf->s_sarr)
	{
		if (inf->sym_arr[i].sym_type == N_UNDF && inf->sym_arr[i].ext &&
			inf->sym_arr[i].addr != 0)
		{
			print_hex(inf->sym_arr[i].addr, inf->arc == X64 ? 16 : 8);
			ft_putstr(" C ");
		}
		else if (inf->sym_arr[i].sym_type == N_UNDF)
			(inf->arc == X64) ? ft_putstr("                 U ") :
								ft_putstr("         U ");
		else
			print_sect_symbol(&inf->sym_arr[i], &inf->sect, inf->arc);
		ft_putendl(inf->sym_arr[i].p);
		++i;
	}
}

void		format_out_nm(t_all_info *inf)
{
	if (inf->f_nbr > 2 && inf->f_type != 2)
	{
		ft_putchar('\n');
		if (inf->f_type == 1)
		{
			ft_putstr(inf->f.file_name);
			ft_putchar('(');
			ft_putstr(inf->obj_name);
			ft_putchar(')');
		}
		else
			ft_putstr(inf->f.file_name);
		ft_putstr(":\n");
	}
	print_nm(inf);
}

void		print_out(t_all_info *inf)
{
	if (inf->op == NM_OP && inf->sym_arr != NULL)
	{
		sort_sym_tab(inf);
		format_out_nm(inf);
		free(inf->sym_arr);
	}
	else
		format_out_otool(inf);
	inf->sym_arr = NULL;
	inf->s_sarr = 0;
	ft_bzero(&inf->sect, sizeof(inf->sect));
	ft_bzero(&inf->text_sect, sizeof(inf->text_sect));
}
