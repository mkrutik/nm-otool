/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ar.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:14:01 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:17:14 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static int			g_n_size(const char *name)
{
	if (!name)
		return (0);
	return (ft_atoi(name + ft_strlen(AR_EFMT1)));
}

static const char	*get_name(struct ar_hdr *head)
{
	if (!ft_strncmp(head->ar_name, AR_EFMT1, ft_strlen(AR_EFMT1)))
		return (void*)head + sizeof(*head);
	return (head->ar_name);
}

static void			p_rlib86(t_all_info *inf, struct ranlib *ell, int n,
							const char *type)
{
	struct ar_hdr	*head;
	int				i;

	i = -1;
	inf->ob_arr = (t_arch_el*)malloc(sizeof(t_arch_el) * n);
	inf->nbr_obj = n;
	ft_bzero(inf->ob_arr, sizeof(t_arch_el) * n);
	while (++i < n)
	{
		head = inf->f.d + ell[i].ran_off;
		inf->ob_arr[i].f_name = get_name(head);
		inf->ob_arr[i].f_offs = ell[i].ran_off;
		inf->ob_arr[i].file = (void*)head + sizeof(*head) +
			g_n_size(head->ar_name);
	}
	proc_and_print(inf, type);
	free(inf->ob_arr);
	inf->ob_arr = NULL;
}

static void			p_rlib64(t_all_info *inf, struct ranlib_64 *ell,
								int n, const char *type)
{
	struct ar_hdr	*head;
	int				i;

	i = -1;
	inf->ob_arr = (t_arch_el*)malloc(sizeof(t_arch_el) * n);
	inf->nbr_obj = n;
	ft_bzero(inf->ob_arr, sizeof(t_arch_el) * n);
	while (++i < n)
	{
		head = inf->f.d + ell[i].ran_off;
		inf->ob_arr[i].f_name = get_name(head);
		inf->ob_arr[i].f_offs = ell[i].ran_off;
		inf->ob_arr[i].file = (void*)head + sizeof(*head) +
			g_n_size(head->ar_name);
	}
	proc_and_print(inf, type);
	free(inf->ob_arr);
	inf->ob_arr = NULL;
}

void				archive(t_all_info *inf)
{
	struct ar_hdr	*head;
	const char		*type;
	size_t			rlib_s;
	int				n;
	void			*ptr;

	inf->f_type = 1;
	head = (struct ar_hdr*)(inf->f.d + SARMAG);
	type = (void*)head + sizeof(*head);
	rlib_s = (!ft_strcmp(type, SYMDEF) || !ft_strcmp(type, SYMDEF_SORTED)) ?
			sizeof(struct ranlib) : sizeof(struct ranlib_64);
	n = *(int*)((void*)head + sizeof(*head) + g_n_size(head->ar_name)) / rlib_s;
	ptr = ((void*)head + sizeof(*head) + g_n_size(head->ar_name) + 4);
	if (!ft_strcmp(type, SYMDEF) || !ft_strcmp(type, SYMDEF_SORTED))
		p_rlib86(inf, ptr, n, type);
	else if (!ft_strcmp(type, SYMDEF_64) || !ft_strcmp(type, SYMDEF_64_SORTED))
		p_rlib64(inf, ptr, n, type);
	else
		return ((void)error(ERROR_UN_MAG, inf->f.file_name));
}
