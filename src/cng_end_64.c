/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cng_end_64.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:18:39 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:19:35 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

struct mach_header_64		end_mach64(struct mach_header_64 *head, BOOL cng)
{
	struct mach_header_64	res;

	res = *head;
	if (cng)
	{
		cng_end((uint8_t*)&res.ncmds, sizeof(res.ncmds));
		cng_end((uint8_t*)&res.sizeofcmds, sizeof(res.sizeofcmds));
		cng_end((uint8_t*)&res.cputype, sizeof(res.cputype));
	}
	return (res);
}

struct section_64			end_sec64(struct section_64 *sec, BOOL cng)
{
	struct section_64	res;

	res = *sec;
	if (cng)
	{
		cng_end((uint8_t*)&res.addr, sizeof(res.addr));
		cng_end((uint8_t*)&res.size, sizeof(res.size));
		cng_end((uint8_t*)&res.offset, sizeof(res.offset));
	}
	return (res);
}

struct segment_command_64	end_seg64(struct segment_command_64 *seg, BOOL cng)
{
	struct segment_command_64 res;

	res = *seg;
	if (cng)
	{
		cng_end((uint8_t*)&res.nsects, sizeof(res.nsects));
		cng_end((uint8_t*)&res.cmdsize, sizeof(res.cmdsize));
	}
	return (res);
}
