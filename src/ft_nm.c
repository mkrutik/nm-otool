/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:26:22 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:26:33 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int	main(int argc, char **argv)
{
	t_all_info		inf;
	int				i;

	i = 0;
	while (++i < argc || argc == 1)
	{
		ft_bzero((void*)&inf, sizeof(inf));
		inf.p_fname = (argc > 2) ? 1 : 0;
		inf.op = NM_OP;
		inf.f_nbr = argc;
		if (initialization((argc > 1) ? argv[i] : "a.out", &(inf.f)) < 0)
			return (EXIT_FAILURE);
		processing(&inf, inf.f.d);
		release_mem(&(inf.f));
		if (argc == 1)
			break ;
	}
	return (EXIT_SUCCESS);
}
