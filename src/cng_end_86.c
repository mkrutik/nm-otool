/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cng_end_86.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:34:16 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:34:55 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

struct mach_header		end_mach86(struct mach_header *head, BOOL cng)
{
	struct mach_header res;

	res = *head;
	if (cng)
	{
		cng_end((uint8_t*)&res.ncmds, sizeof(res.ncmds));
		cng_end((uint8_t*)&res.sizeofcmds, sizeof(res.sizeofcmds));
		cng_end((uint8_t*)&res.cputype, sizeof(res.cputype));
	}
	return (res);
}

struct section			end_sec86(struct section *sec, BOOL cng)
{
	struct section res;

	res = *sec;
	if (cng)
	{
		cng_end((uint8_t*)&res.addr, sizeof(res.addr));
		cng_end((uint8_t*)&res.size, sizeof(res.size));
		cng_end((uint8_t*)&res.offset, sizeof(res.offset));
	}
	return (res);
}

struct segment_command	end_seg86(struct segment_command *seg, BOOL cng)
{
	struct segment_command res;

	res = *seg;
	if (cng)
	{
		cng_end((uint8_t*)&res.nsects, sizeof(res.nsects));
		cng_end((uint8_t*)&res.cmdsize, sizeof(res.cmdsize));
	}
	return (res);
}
