/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_worker.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:23:43 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:24:05 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		initialization(const char *file_name, t_file_info *out)
{
	struct stat	sb;

	out->fd = open(file_name, O_RDONLY);
	if (out->fd < 0)
		return (error(ERROR_OPEN, file_name));
	if (fstat(out->fd, &sb) == -1)
		return (error(ERROR_FSTAT, file_name));
	if (!S_ISREG(sb.st_mode))
		return (error(ERROR_ISNT, file_name));
	out->f_len = sb.st_size;
	out->file_name = file_name;
	out->mem_len = sb.st_size + sb.st_size % getpagesize();
	out->d = mmap(NULL, out->mem_len, PROT_READ, MAP_PRIVATE,
								out->fd, 0);
	if (out->d == MAP_FAILED)
		return (error(ERROR_MMAP, file_name));
	out->end_data = out->d + out->f_len;
	return (0);
}

void	release_mem(const t_file_info *file)
{
	if (munmap(file->d, file->mem_len) == -1)
		error(ERROR_UNMMAP, file->file_name);
	else if (close(file->fd) == -1)
		error(ERROR_CLOSE, file->file_name);
}
