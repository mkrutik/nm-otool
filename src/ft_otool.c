/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:26:47 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:27:16 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	usage(const char *f_name)
{
	ft_putstr(f_name);
	ft_putstr(" : ERROR at least one file must be specified\n");
}

int		main(int argc, char **argv)
{
	t_all_info		inf;
	int				i;

	if (argc == 1)
	{
		usage(argv[0]);
		return (EXIT_SUCCESS);
	}
	i = 0;
	while (++i < argc || argc == 1)
	{
		ft_bzero((void*)&inf, sizeof(inf));
		inf.p_fname = (argc > 2) ? 1 : 0;
		inf.op = OTL_OP;
		inf.f_nbr = argc;
		if (initialization(argv[i], &(inf.f)) < 0)
			return (EXIT_FAILURE);
		processing(&inf, inf.f.d);
		release_mem(&(inf.f));
		if (argc == 1)
			break ;
	}
	return (EXIT_SUCCESS);
}
