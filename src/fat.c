/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fat.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:22:05 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:23:30 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	fat86(t_all_info *inf)
{
	const struct fat_arch	*arch;
	uint32_t				nbr;
	cpu_type_t				type;

	inf->f_type = 2;
	if (inf->f.f_len < sizeof(struct fat_header))
		return ((void)error(ERROR_FFORMAT, inf->f.file_name));
	nbr = c_end32(&((struct fat_header *)inf->f.d)->nfat_arch, inf->swap);
	if ((sizeof(struct fat_header) + (sizeof(struct fat_arch) * nbr)) >
		inf->f.f_len)
		return ((void)error(ERROR_FFORMAT, inf->f.file_name));
	arch = (struct fat_arch*)(inf->f.d + sizeof(struct fat_header));
	while (nbr--)
	{
		type = arch->cputype;
		(inf->swap) ? cng_end((uint8_t*)&type, sizeof(type)) : 0;
		if ((type == CPU_TYPE_X86_64 && sizeof(void*) == 8) ||
			(type == CPU_TYPE_I386 && sizeof(void*) == 4))
			processing(inf, inf->f.d + c_end32((uint32_t*)&arch->offset,
						inf->swap));
		arch++;
	}
}

void	fat64(t_all_info *inf)
{
	const struct fat_arch_64	*arch;
	uint32_t					nbr;
	cpu_type_t					type;

	inf->f_type = 2;
	if (inf->f.f_len < sizeof(struct fat_header))
		return ((void)error(ERROR_FFORMAT, inf->f.file_name));
	nbr = c_end32(&((struct fat_header*)inf->f.d)->nfat_arch, inf->swap);
	if ((sizeof(struct fat_header) + (sizeof(struct fat_arch_64) * nbr)) >
		inf->f.f_len)
		return ((void)error(ERROR_FFORMAT, inf->f.file_name));
	arch = (struct fat_arch_64*)(inf->f.d + sizeof(struct fat_header));
	while (nbr--)
	{
		type = arch->cputype;
		(inf->swap) ? cng_end((uint8_t*)&type, sizeof(type)) : 0;
		if ((type == CPU_TYPE_X86_64 && sizeof(void*) == 8) ||
			(type == CPU_TYPE_I386 && sizeof(void*) == 4))
			processing(inf, inf->f.d + c_end64((uint64_t*)&arch->offset,
						inf->swap));
		arch++;
	}
}
