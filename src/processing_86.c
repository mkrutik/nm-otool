/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_86.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:30:00 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:31:12 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static void		s_tab86(t_all_info *inf, const void *ptr, const void *file)
{
	struct symtab_command	sc;
	struct nlist			*cmd_array;
	ssize_t					i;

	sc = end_symtab((struct symtab_command *)ptr, inf->swap);
	if ((sc.stroff + sc.strsize) > inf->f.f_len ||
		(sc.symoff + sc.nsyms * sizeof(struct nlist)) > inf->f.f_len)
		return ((void)(error(ERROR_FSIZE, inf->f.file_name)));
	inf->string_tab = (void *)(file + sc.stroff);
	cmd_array = (struct nlist *)(file + sc.symoff);
	inf->sym_arr = (t_sym_row *)malloc(sizeof(t_sym_row) * sc.nsyms);
	inf->s_sarr = 0;
	i = -1;
	while (++i < sc.nsyms)
		if (!(cmd_array[i].n_type & N_STAB))
			add_symbl(inf, (void*)&(cmd_array[i]));
}

static void		p_sect_86(t_all_info *inf, const void *ptr, const void *file)
{
	struct section			sec;
	struct segment_command	seg;
	ssize_t					i;

	seg = end_seg86((struct segment_command*)ptr, inf->swap);
	if ((ptr + seg.cmdsize) > inf->f.end_data)
		return ((void)error(ERROR_FFORMAT, inf->f.file_name));
	ptr += sizeof(seg);
	sec = end_sec86((struct section*)ptr, inf->swap);
	i = -1;
	while (++i < seg.nsects)
	{
		if (!ft_strcmp(sec.sectname, SECT_BSS))
			inf->sect.bss = inf->sect.index + 1;
		else if (!ft_strcmp(sec.sectname, SECT_TEXT))
		{
			inf->sect.text = inf->sect.index + 1;
			fill_text_sec(inf, &sec, file);
		}
		else if (!ft_strcmp(sec.sectname, SECT_DATA))
			inf->sect.data = inf->sect.index + 1;
		sec = *(struct section*)(ptr + (sizeof(sec) * (i + 1)));
		inf->sect.index++;
	}
}

void			mach86(t_all_info *inf, const void *file)
{
	struct mach_header		head;
	struct load_command		lc;
	const void				*ptr;

	ptr = file;
	if ((ptr + sizeof(head)) > inf->f.end_data)
		return ((void)(error(ERROR_FSIZE, inf->f.file_name)));
	head = end_mach86((struct mach_header*)ptr, inf->swap);
	if ((ptr + sizeof(head) + head.sizeofcmds) > inf->f.end_data)
		return (void)(error(ERROR_FSIZE, inf->f.file_name));
	ptr += sizeof(struct mach_header);
	lc = end_lc((struct load_command *)ptr, inf->swap);
	while (head.ncmds--)
	{
		if (lc.cmd == LC_SEGMENT)
			p_sect_86(inf, ptr, file);
		else if (inf->op == NM_OP && lc.cmd == LC_SYMTAB)
			s_tab86(inf, ptr, file);
		ptr += lc.cmdsize;
		lc = end_lc((struct load_command *)ptr, inf->swap);
	}
	print_out(inf);
}
