/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprint_otool.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:25:34 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:26:11 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static void	print_otool(t_all_info *inf)
{
	size_t		ms;
	uint64_t	i;

	if (inf->text_sect.size != 0)
	{
		if (inf->text_sect.data + inf->text_sect.size > inf->f.end_data)
			return ((void)error(ERROR_FFORMAT, inf->f.file_name));
		ms = inf->arc == X64 ? 16 : 8;
		while (inf->text_sect.size)
		{
			print_hex(inf->text_sect.addr, ms);
			ft_putstr("	");
			i = 0;
			while (i++ < ms && inf->text_sect.size != 0)
			{
				print_hex(*((char *)inf->text_sect.data++), 2);
				ft_putchar(' ');
				inf->text_sect.size--;
			}
			inf->text_sect.addr += ms;
			ft_putstr("\n");
		}
	}
}

void		format_out_otool(t_all_info *inf)
{
	if (inf->f_type == 1)
	{
		ft_putstr(inf->f.file_name);
		ft_putchar('(');
		ft_putstr(inf->obj_name);
		ft_putstr("):\n");
	}
	else
	{
		ft_putstr(inf->f.file_name);
		ft_putstr(":\n");
	}
	ft_putstr("Contents of (__TEXT,__text) section\n");
	print_otool(inf);
}
