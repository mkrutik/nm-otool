/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ar_helper.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:17:28 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:18:17 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static void		ob_sort(t_all_info *inf)
{
	ssize_t		i;
	t_arch_el	tmp;
	BOOL		swaped;

	swaped = TRUE;
	while (swaped)
	{
		i = -1;
		swaped = FALSE;
		while ((size_t)(++i + 1) < inf->nbr_obj)
			if (ft_strcmp(inf->ob_arr[i].f_name, inf->ob_arr[i + 1].f_name) > 0)
			{
				tmp = inf->ob_arr[i];
				inf->ob_arr[i] = inf->ob_arr[i + 1];
				inf->ob_arr[i + 1] = tmp;
				swaped = TRUE;
			}
	}
}

void			proc_and_print(t_all_info *inf, const char *type)
{
	size_t i;

	i = 0;
	if (inf->nbr_obj > 0 && (!ft_strcmp(type, SYMDEF) ||
							!ft_strcmp(type, SYMDEF_64)))
		ob_sort(inf);
	if (inf->op == OTL_OP)
	{
		ft_putstr("Archive : ");
		ft_putendl(inf->f.file_name);
	}
	while (i < inf->nbr_obj)
	{
		inf->obj_name = inf->ob_arr[i].f_name;
		processing(inf, inf->ob_arr[i].file);
		inf->obj_name = NULL;
		i++;
	}
}
