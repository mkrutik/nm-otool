/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sym_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:31:21 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:33:44 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	fill_text_sec(t_all_info *i, const void *section, const void *file)
{
	if (i->arc == X64)
	{
		i->text_sect.size = c_end64(&((struct section_64*)section)->size,
									i->swap);
		i->text_sect.data = file +
					c_end32(&((struct section_64*)section)->offset, i->swap);
		i->text_sect.addr = c_end64(&((struct section_64*)section)->addr,
									i->swap);
		return ;
	}
	i->text_sect.size = c_end32(&((struct section*)section)->size, i->swap);
	i->text_sect.data = file + c_end32(&((struct section*)section)->offset,
										i->swap);
	i->text_sect.addr = c_end32(&((struct section*)section)->addr, i->swap);
}

void	add_symbl(t_all_info *inf, void *ptr)
{
	inf->sym_arr[inf->s_sarr].p = inf->string_tab +
		c_end32(&((struct nlist*)ptr)->n_un.n_strx, inf->swap);
	inf->sym_arr[inf->s_sarr].sec_num = ((struct nlist*)ptr)->n_sect;
	inf->sym_arr[inf->s_sarr].sym_type = ((struct nlist*)ptr)->n_type & N_TYPE;
	inf->sym_arr[inf->s_sarr].ext = ((struct nlist*)ptr)->n_type & N_EXT;
	inf->sym_arr[inf->s_sarr].addr = (inf->arc == X64) ?
		c_end64(&((struct nlist_64*)ptr)->n_value, inf->swap) :
		c_end32(&((struct nlist*)ptr)->n_value, inf->swap);
	inf->s_sarr++;
}

void	sort_sym_tab(t_all_info *inf)
{
	ssize_t		i;
	t_sym_row	tmp;
	BOOL		swaped;

	swaped = TRUE;
	while (swaped)
	{
		i = -1;
		swaped = FALSE;
		while ((size_t)(++i + 1) < inf->s_sarr)
			if (ft_strcmp(inf->sym_arr[i].p, inf->sym_arr[i + 1].p) > 0)
			{
				tmp = inf->sym_arr[i];
				inf->sym_arr[i] = inf->sym_arr[i + 1];
				inf->sym_arr[i + 1] = tmp;
				swaped = TRUE;
			}
	}
}
