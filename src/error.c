/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:21:37 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:21:52 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		error(t_error_code code, const char *f_name)
{
	ft_putstr(f_name);
	ft_putstr(": ");
	if (code == ERROR_OPEN)
		ft_putstr(ERROR_OPEN_MSG);
	else if (code == ERROR_FSTAT)
		ft_putstr(ERROR_FSTAT_MSG);
	else if (code == ERROR_ISNT)
		ft_putstr(ERROR_ISNT_MSG);
	else if (code == ERROR_MMAP)
		ft_putstr(ERROR_MMAP_MSG);
	else if (code == ERROR_UNMMAP)
		ft_putstr(ERROR_UNMMAP_MSG);
	else if (code == ERROR_CLOSE)
		ft_putstr(ERROR_CLOSE_MSG);
	else if (ERROR_FSIZE)
		ft_putstr(ERROR_FSIZE_MSG);
	else if (ERROR_FFORMAT)
		ft_putstr(ERROR_FFORMAT_MSG);
	else if (ERROR_UN_MAG)
		ft_putstr(ERROR_UN_MAG_MSG);
	return ((int)code);
}
