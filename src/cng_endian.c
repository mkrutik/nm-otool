/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cng_endian.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 10:20:02 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:21:27 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

struct symtab_command	end_symtab(struct symtab_command *st, BOOL cng)
{
	struct symtab_command	res;

	res = *st;
	if (cng)
	{
		cng_end((uint8_t *)&res.cmd, sizeof(res.cmd));
		cng_end((uint8_t *)&res.symoff, sizeof(res.symoff));
		cng_end((uint8_t *)&res.nsyms, sizeof(res.nsyms));
		cng_end((uint8_t *)&res.stroff, sizeof(res.stroff));
		cng_end((uint8_t *)&res.strsize, sizeof(res.strsize));
	}
	return (res);
}

struct load_command		end_lc(struct load_command *lc, BOOL cng)
{
	struct load_command	res;

	res = *lc;
	if (cng)
	{
		cng_end((uint8_t*)&res.cmd, sizeof(res.cmd));
		cng_end((uint8_t*)&res.cmdsize, sizeof(res.cmdsize));
	}
	return (res);
}

uint32_t				c_end32(uint32_t *p, BOOL is_need)
{
	uint32_t	copy;

	if (is_need == FALSE)
		return (*p);
	copy = *p;
	cng_end((uint8_t*)&copy, sizeof(uint32_t));
	return (copy);
}

uint64_t				c_end64(uint64_t *p, BOOL is_need)
{
	uint64_t	copy;

	if (is_need == FALSE)
		return (*p);
	copy = *p;
	cng_end((uint8_t*)&copy, sizeof(uint64_t));
	return (copy);
}

void					cng_end(uint8_t *array, size_t len)
{
	size_t	i;
	uint8_t	tmp;

	if (array == NULL || len < 2)
		return ;
	i = 0;
	len -= 1;
	while (i < len)
	{
		tmp = array[i];
		array[i] = array[len];
		array[len] = tmp;
		++i;
		--len;
	}
}
