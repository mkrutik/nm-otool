/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:08:39 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:35:30 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *src1, const char *src2)
{
	unsigned char *s1;
	unsigned char *s2;

	if (!src1 || !src2)
		return (0);
	s1 = (unsigned char*)src1;
	s2 = (unsigned char*)src2;
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}
