/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 09:58:20 by mkrutik           #+#    #+#             */
/*   Updated: 2019/01/22 10:10:49 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include "libft.h"

# include <stdint.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/mman.h>

# include <ar.h>
# include <mach-o/fat.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/ranlib.h>

# define BOOL		uint8_t
# define TRUE		1
# define FALSE		0

# define X86		0x1
# define X64		0x2

# define NM_OP 		0x10
# define OTL_OP		0x20

# define ERROR_OPEN_MSG		"ERROR: can't open file\n"
# define ERROR_FSTAT_MSG	"ERROR: can't get file status information\n"
# define ERROR_ISNT_MSG		"ERROR: It isn't a regular file\n"
# define ERROR_MMAP_MSG		"ERROR: can't mapping file\n"
# define ERROR_UNMMAP_MSG	"ERROR: can't unmap file\n"
# define ERROR_CLOSE_MSG	"ERROR: can't close file\n"
# define ERROR_FSIZE_MSG 	"ERROR: brocken file - insufficient size\n"
# define ERROR_FFORMAT_MSG 	"ERROR: unsupported file format\n"
# define ERROR_UN_MAG_MSG	"ERROR: File format not recognized\n"

typedef enum				e_error_code {
	ERROR_OPEN = -1,
	ERROR_FSTAT = -2,
	ERROR_ISNT = -3,
	ERROR_MMAP = -4,
	ERROR_UNMMAP = -5,
	ERROR_CLOSE = -6,
	ERROR_FFORMAT = -7,
	ERROR_FSIZE = -8,
	ERROR_UN_MAG = -9
}							t_error_code;

typedef	struct				s_sym_row {
	uint64_t				addr;
	uint8_t					sym_type;
	uint8_t					sec_num;
	uint8_t					ext;
	const char				*p;
}							t_sym_row;

typedef struct				s_file_info {
	const char				*file_name;
	int						fd;
	size_t					f_len;
	size_t					mem_len;
	void					*d;
	void					*end_data;
}							t_file_info;

typedef struct				s_sections {
	size_t					bss;
	size_t					data;
	size_t					text;
	size_t					index;
}							t_sect;

typedef struct				s_arch_el {
	const void				*file;
	const char				*f_name;
	uint64_t				f_offs;
}							t_arch_el;

typedef struct				s_text_seg {
	const void				*data;
	uint64_t				size;
	uint64_t				addr;
}							t_text_seg;

typedef struct				s_parse_info {
	t_file_info				f;
	BOOL					p_fname;
	BOOL					swap;
	uint8_t					op;
	uint8_t					arc;
	uint8_t					f_type;
	const char				*obj_name;
	uint8_t					f_nbr;

	const char				*string_tab;
	t_sym_row				*sym_arr;
	size_t					s_sarr;

	t_sect					sect;
	t_text_seg				text_sect;

	t_arch_el				*ob_arr;
	size_t					nbr_obj;
}							t_all_info;

/*
** general functions
*/
int							initialization(const char *f, t_file_info *out_inf);
void						release_mem(const t_file_info *file);
int							error(t_error_code code, const char *f_name);
BOOL						need_cng_end(uint32_t magic);
int							processing(t_all_info *inf, const void *ptr);
/*
** Mach-O obect file format processing
*/
void						mach86(t_all_info *inf, const void *ptr);
void						mach64(t_all_info *inf, const void *ptr);
/*
** Fat (Multi-CPU Architecture Files) file format processing
*/
void						fat64(t_all_info *inf);
void						fat86(t_all_info *inf);
/*
** Archive (static library) file format processing
*/
void						archive(t_all_info *inf);
void						proc_and_print(t_all_info *inf, const char *type);
/*
** Formated output functions
*/
void						print_out(t_all_info *inf);
void						print_hex(uint64_t addr, size_t align);
void						format_out_otool(t_all_info *inf);

/*
** Utils function
*/
void						add_symbl(t_all_info *inf, void *ptr);
void						sort_sym_tab(t_all_info *inf);
void						fill_text_sec(t_all_info *inf, const void *section,
										const void *file);
/*
** Change endian functions
*/
void						cng_end(uint8_t *array, size_t len);
uint64_t					c_end64(uint64_t *p, BOOL is_need);
uint32_t					c_end32(uint32_t *p, BOOL is_need);
struct load_command			end_lc(struct load_command *lc, BOOL cng);
struct symtab_command		end_symtab(struct symtab_command *st, BOOL cng);
struct mach_header_64		end_mach64(struct mach_header_64 *head, BOOL cng);
struct section_64			end_sec64(struct section_64 *sec, BOOL cng);
struct segment_command_64	end_seg64(struct segment_command_64 *s, BOOL cng);
struct mach_header			end_mach86(struct mach_header *head, BOOL cng);
struct section				end_sec86(struct section *sec, BOOL cng);
struct segment_command		end_seg86(struct segment_command *seg, BOOL cng);

#endif
