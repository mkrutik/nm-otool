RED 	= \033[0;31m
GREEN 	= \033[1;32m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m

CC			= clang
CC_FLAGS	= -Wall -Wextra -Werror -g #-m32

BIN			= bin
INCLUDE		= -I include/ -I libft/
LIBRARY		= -L./libft/ -lft

SRC = 	src/file_worker.c \
		src/parse_header.c \
		src/fprint_nm.c \
		src/fprint_otool.c \
		src/error.c \
		src/processing_64.c \
		src/processing_86.c \
		src/fat.c \
		src/ar.c \
		src/ar_helper.c \
		src/cng_endian.c \
		src/cng_end_64.c \
		src/cng_end_86.c \
		src/sym_utils.c

SRC_NM			= $(SRC) src/ft_nm.c
OBJ_NM			= $(SRC_NM:.c=.o)
EXECUTABLE_NM	= $(BIN)/ft_nm

SRC_OTOOL 		 = $(SRC) src/ft_otool.c
OBJ_OTOOL 		 = $(SRC_OTOOL:.c=.o)
EXECUTABLE_OTOOL = $(BIN)/ft_otool


all: compile_lib create_bin $(EXECUTABLE_NM) $(EXECUTABLE_OTOOL)
	@ echo "${GREEN}Compile binary file was successful${DEF}"

create_bin:
	@ mkdir -p bin

compile_lib:
	@ make -s -C libft/

$(EXECUTABLE_NM): $(OBJ_NM)
	@ $(CC) $(CC_FLAGS) $(OBJ_NM) -o $(EXECUTABLE_NM) $(LIBRARY)

$(EXECUTABLE_OTOOL): $(OBJ_OTOOL)
	@ $(CC) $(CC_FLAGS) $(OBJ_OTOOL) -o $(EXECUTABLE_OTOOL) $(LIBRARY)

%.o : %.c
	@ $(CC) $(CC_FLAGS) $(INCLUDE) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -f $(OBJ_NM) $(OBJ_OTOOL)
	@ make clean -s -C libft/
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -f $(EXECUTABLE_NM) $(EXECUTABLE_OTOOL)
	@ rm -rf $(BIN)
	@ make fclean -s -C libft/
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all